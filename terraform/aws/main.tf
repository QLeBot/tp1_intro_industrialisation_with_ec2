provider "aws" {
	region = "eu-west-1"
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDRfVmGVpw/ZVEujNnzr06RFYonC+lWrvqzuOg0uqTap9IKsGgS3SOE5VauVCV7pzfc7RpnoEI2BvxOXfsT/1wpXMX8cwX4ku1dEVjE+aCnLbdcQeh67hrelJgyxqhsx1hCLrEMTV3xtZDwKa5ojxpVzzrfVcL7F2vjduuAn0Rj8JPZVhCterFs5v9wzKE7OsZyqVAfX85NsWM5MH/GIlumJYlafMWI2xRClqcC0cNzHUNRl2WFIR9IhH5a9doCerLl9Ck3x0twghWiOI8rDGCHyDDvEe8fDetR6EKzvnghhFf7F/hCMD/ebjA6QytxQ35YLPrZBq6su8R2IbYctEikCK0G9MlL2dw0vJcYjLSaYAt6WjHzVhbQrsycCdXCiH9eHL2QZczLFYUkRHxo5pSeOah9CrtC14/sRXu2DaNOapuPHKgNSsGhx9Zy2y3s85Vd0QfTG4AH4tRA8twIuXzRxQ1gaDsEBBqr2RxRoOYJhJjamJcEcBS+jAHrZYQWvCE="
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_vpc" "mainvpc" {
  cidr_block = "10.1.0.0/16"
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.mainvpc.id

  ingress {
    protocol  = -1
    self      = true
    from_port = 22
    to_port   = 80
  }

  ingress {
    protocol  = -1
    self      = true
    from_port = 0
    to_port   = 0
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "my-ec2" {
  ami           = var.ami_id
  instance_type = "t2.micro"
  key_name      = "deployer-key"

  tags = {
    Name = var.tag_name
  }
  depends_on = [aws_key_pair.deployer]
}